val scala3Version = "3.0.0"

lazy val root = project
  .in(file("."))
  .settings(resolvers += "clojars" at "https://repo.clojars.org")
  .settings(
    name := "scala3-simple",
    version := "0.1.0",

    scalaVersion := scala3Version,
    libraryDependencies ++= Seq("com.novocode" % "junit-interface" % "0.11" % "test",
    "com.jsyn" % "jsyn" % "20170815")

  )
