import com.jsyn.JSyn
import com.jsyn.Synthesizer
import com.jsyn.unitgen.{SawtoothOscillatorBL,SineOscillator, SquareOscillator, TriangleOscillator}
import com.jsyn.unitgen.UnitGenerator
import com.jsyn.unitgen.LineOut
import com.jsyn.unitgen.UnitOscillator

enum OscillatorTypes:
  case Sine, Square, Sawtooth, OlSkoo, Triangle

class SineSynth extends SynthWrapper:
    
    def setup(): Unit =
        println(s"Setting up oscillator ${this.lineOut.isEnabled}")
        val sineOsc = new SineOscillator(345, 0.6)
        println(s"lineout ${this.lineOut.getUnitGenerator.isEnabled}")
        synth.add(sineOsc)
        println(s"added osc ${this.synth.isRunning}")
        this.oscillators = Seq(sineOsc)
        
class TriangleSynth extends SynthWrapper:

    def setup(): Unit =
        val triangleOsc = new TriangleOscillator()
        triangleOsc.frequency.set(123)
        triangleOsc.amplitude.set(0.6)
        synth.add(triangleOsc)
        this.oscillators = Seq(triangleOsc)

class SawtoothSynth extends SynthWrapper:

    def setup(): Unit =
        val sawtoothOsc = new SawtoothOscillatorBL()
        sawtoothOsc.frequency.set(345)
        sawtoothOsc.amplitude.set(0.6)
        synth.add(sawtoothOsc)
        this.oscillators = Seq(sawtoothOsc)
        