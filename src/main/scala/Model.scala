import com.jsyn.JSyn
import com.jsyn.Synthesizer
import com.jsyn.unitgen.UnitGenerator
import com.jsyn.unitgen.LineOut
import com.jsyn.unitgen.UnitOscillator

abstract case class SynthWrapper(val synth: Synthesizer=JSyn.createSynthesizer, var oscillators: Seq[UnitOscillator] = Seq(), val lineOut:LineOut = new LineOut):

  def connectOutput(): Unit =
    println(s"Connecting output... ${lineOut.hashCode}")
    for o <- this.oscillators do
        println(s"osc ${o.getClass}")
        o.output.connect(0, this.lineOut.input, 0)
        o.output.connect(0, this.lineOut.input, 1)

  def initializeAndStart(): Unit =
    synth.start()
    synth.add(lineOut)
    println(s"initializing... ${lineOut.hashCode}")
    setup()
    connectOutput()
    start()

  def start(): Unit = 
    println(s"Starting lineout...${lineOut.hashCode}")
    lineOut.start()

  def stop(): Unit =
    lineOut.stop()
    synth.stop()

  /**
   * Create synth object, add oscillators, call setup on oscillators, etc...
   */  
  def setup(): Unit
