import com.jsyn.JSyn
import com.jsyn.unitgen._
import com.jsyn.devices._
import OscillatorTypes._

@main def audio_stutter(synthType: String, durationInSeconds: Int): Unit = 
    val durInMillis = 1000 * durationInSeconds
    OscillatorTypes.valueOf(synthType) match
        case Sine =>
            val s  = SineSynth()
            s.initializeAndStart()
            Thread.sleep(durInMillis)
            s.stop()
        case Triangle =>
            val s  = TriangleSynth()
            s.initializeAndStart()
            Thread.sleep(durInMillis)
            s.stop()
        case Sawtooth =>
            val s  = SawtoothSynth()
            s.initializeAndStart()
            Thread.sleep(durInMillis)
            s.stop()
        case OlSkoo =>
            val s = JSyn.createSynthesizer
            val lo  = new LineOut
            s.start()
            s.add(lo)
            val sqOs= new SquareOscillator
            sqOs.frequency.set(120)
            sqOs.amplitude.set(0.6)
            sqOs.output.connect(0, lo.input, 0)
            sqOs.output.connect(0, lo.input, 1)
            s.add(sqOs)
            lo.start()
            Thread.sleep(10000)
            lo.stop()
            s.stop()
        case _ =>
            println(s"Unsupported Synth Type provided. Try one of these: ${OscillatorTypes.values}")

    System.exit(0)
