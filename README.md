## PremierSynth

This project is an Audio synthesis library that is focused on 
running in the Scala 3 ecosystem. 

## Requisites

- PulseAudio setup in the runtime environment (JSyn has support for Suns audio engine but this is a tedious installation step)

### Usage

The test application is invoked via SBT like this:

```
sbt "run <Sine|Sawtooth|Triangle|OlSkoo>"
```

The un-named arg specifies the Synth type to use. 

### Synths

- Sine - a Sine synth
- Sawtooth - a Sawtooth synth
- Triangle - a Triangle synth
- OlSkoo - a Square synth
  
